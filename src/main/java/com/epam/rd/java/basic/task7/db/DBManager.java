package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class DBManager {

    private static DBManager instance;
    private Connection con;
    private Statement stm;
    private PreparedStatement prstm;
    private final String SQL_QUERY_FIND_ALL_USERS = "SELECT login FROM users";
    private final String SQL_QUERY_FIND_ALL_TEAMS = "SELECT name FROM teams";
    private final String SQL_INSERT_USER_QUERY = "INSERT INTO users VALUES (DEFAULT,?)";
    private final String SQL_INSERT_TEAM_QUERY = "INSERT INTO teams VALUES (DEFAULT,?)";
    private final String SQL_DELETE_USERS_QUERY = "DELETE FROM users WHERE login=?";


    public static synchronized DBManager getInstance() {
        instance = new DBManager();
        return instance;
    }

    private DBManager() {
    }

    public void getConnection() {
        try (FileReader reader = new FileReader("app.properties")) {
            Properties prop = new Properties();
            prop.load(reader);
            con = DriverManager.getConnection(prop.getProperty("connection.url"));
        } catch (IOException | SQLException exception) {
            exception.printStackTrace();
        }
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        getConnection();
        try {
            stm = con.createStatement();
            ResultSet resultSet = stm.executeQuery(SQL_QUERY_FIND_ALL_USERS);
            while (resultSet.next()) {
                String temp_login = resultSet.getString(1);
                users.add(User.createUser(temp_login));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(con);
            close(stm);
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        try {
            getConnection();
            prstm = con.prepareStatement(SQL_INSERT_USER_QUERY, Statement.RETURN_GENERATED_KEYS);
            prstm.setString(1, user.getLogin());
            prstm.execute();
            ResultSet generatedKeys = prstm.getGeneratedKeys();
            if (generatedKeys.next()) {
                user.setId(generatedKeys.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(con);
            close(prstm);
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        try {
            getConnection();
            con.setAutoCommit(false);
            prstm = con.prepareStatement(SQL_DELETE_USERS_QUERY);
            for (User user : users) {
                prstm.setString(1, user.getLogin());
                prstm.execute();
            }
            con.commit();
        } catch (SQLException e) {
            try {
                con.rollback();
            } catch (SQLException exp) {
                e.printStackTrace();
            }
            e.printStackTrace();
            return false;
        } finally {
            try {
                con.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            close(con);
            close(prstm);
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        int id = 0;
        String findLog = null;
        try {
            getConnection();
            prstm = con.prepareStatement("SELECT * FROM users WHERE login=?");
            prstm.setString(1, login);
            ResultSet resultSet = prstm.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt(1);
                findLog = resultSet.getString(2);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(con);
            close(prstm);
        }
        User user = User.createUser(findLog);
        user.setId(id);
        return user;
    }

    public Team getTeam(String name) throws DBException {
        int id = 0;
        String findName = null;
        try {
            getConnection();
            prstm = con.prepareStatement("SELECT * FROM teams WHERE name=?");
            prstm.setString(1, name);
            ResultSet resultSet = prstm.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt(1);
                findName = resultSet.getString(2);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(con);
            close(prstm);
        }
        Team team = Team.createTeam(findName);
        team.setId(id);
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        getConnection();
        try {
            stm = con.createStatement();
            ResultSet resultSet = stm.executeQuery(SQL_QUERY_FIND_ALL_TEAMS);
            while (resultSet.next()) {
                String temp_name = resultSet.getString(1);
                teams.add(Team.createTeam(temp_name));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(con);
            close(stm);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        try {
            getConnection();
            prstm = con.prepareStatement(SQL_INSERT_TEAM_QUERY, Statement.RETURN_GENERATED_KEYS);
            prstm.setString(1, team.getName());
            prstm.execute();
            ResultSet generatedKeys = prstm.getGeneratedKeys();
            if (generatedKeys.next()) {
                team.setId(generatedKeys.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(con);
            close(prstm);
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        try {
            getConnection();
            con.setAutoCommit(false);
            prstm = con.prepareStatement("INSERT INTO users_teams VALUES (?,?)");
            for (Team team : teams) {
                prstm.setInt(1, user.getId());
                prstm.setInt(2, team.getId());
                prstm.execute();
            }
            con.commit();
        } catch (SQLException exp) {
            try {
                con.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            throw new DBException("exeption", exp);
        } finally {
            try {
                con.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            close(con);
            close(prstm);
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        String finderSQL = "SELECT team_id FROM users_teams WHERE user_id=?";
        String teamFinder = "SELECT name FROM teams WHERE id=?";
        getConnection();
        List<Team> teams = new ArrayList<>();
        try {
            con.setAutoCommit(false);
            prstm = con.prepareStatement(finderSQL);
            prstm.setInt(1, user.getId());
            ResultSet resultSet = prstm.executeQuery();
            prstm = con.prepareStatement(teamFinder);
            while (resultSet.next()) {
                prstm.setInt(1, resultSet.getInt(1));
                ResultSet resultSet1 = prstm.executeQuery();
                if (resultSet1.next()) {
                    teams.add(Team.createTeam(resultSet1.getString(1)));
                }
            }
            con.commit();
        } catch (SQLException exp) {
            try {
                con.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            exp.printStackTrace();
        } finally {
            try {
                con.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            close(con);
            close(prstm);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        String deleteFromUsers_Teams = "DELETE FROM users_teams WHERE team_id=?";
        String deleteFromTeams = "DELETE FROM teams WHERE name=?";
        getConnection();
        try {
            con.setAutoCommit(false);
            prstm = con.prepareStatement(deleteFromTeams);
            prstm.setString(1, team.getName());
            prstm.executeUpdate();
            prstm = con.prepareStatement(deleteFromUsers_Teams);
            prstm.setInt(1, team.getId());
            prstm.executeUpdate();
            con.commit();
        } catch (SQLException e) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                con.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                close(con);
                close(prstm);
            }
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        String sqlUpdate = "UPDATE teams SET name=? WHERE id=?";
        getConnection();
        try {
            prstm = con.prepareStatement(sqlUpdate);
            prstm.setString(1, team.getName());
            prstm.setInt(2, team.getId());
            prstm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(con);
            close(prstm);
        }
        return true;
    }

    private static void close(Connection connection) {
        try {
            assert connection != null;
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void close(Statement statement) {
        try {
            assert statement != null;
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
